# go-axiata
Interview for Go Axiata By Kevin Naufal Permana

# How to run
1. Please use "go get" on these items first
    "github.com/lib/pq"
    "github.com/google/uuid"
2. Make sure your local postgresql's live and running
3. Create database to store the tables, please refer or adjust configs to configs/db.go for db connection
4. Run all the migration scripts on to your local postgresql databse
5. make sure again that the configs const in db.go match your local configuration
6. Type "go run ." to start local server, if you intent to make changes make sure to restart
7. The collections of API for interaction can be found [here](https://drive.google.com/file/d/1mp_RuMhXVLupjoC991MMe0xGma1QU3Al/view?usp=sharing)
8. If there's anything regarding the api and source code feel free to reach me out through naufal.kevin7@outlook.com